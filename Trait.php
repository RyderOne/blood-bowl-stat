<?php

namespace BB;

class Trait {

    const TYPE_AGILITY = 1;
    const TYPE_ARMOR = 2;
    const TYPE_STRENGTH = 3;
    const TYPE_MOVMENT = 4;

    public function __construct($type) {
        $this->setType($type);
    }

    /**
     * @var integer
     */
    private $type;

    /**
     * @var integer
     */
    private $value = 0;

    /**
     * Gets the value of type.
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param integer $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of value.
     *
     * @return integer
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Sets the value of value.
     *
     * @param integer $value the value
     *
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
}