<?php

namespace BB;

class Race 
{
    const TYPE_HUMAN = 0;
    const TYPE_ORC = 1;
    const TYPE_DWARF = 2;
    const TYPE_ELVE = 3;
    const TYPE_SKAVEN = 4;

    public function __construct($type) {
        $this->setType($type);
    }

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $type;

    /**
     * @var array
     */
    private $players;

    /**
     * Gets the value of name.
     *
     * @return strin
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of type.
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the value of type.
     *
     * @param integer $type the type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Gets the value of players.
     *
     * @return array
     */
    public function getPlayers()
    {
        return $this->players;
    }

    /**
     * Sets the value of players.
     *
     * @param array $players the players
     *
     * @return self
     */
    public function setPlayers(array $players)
    {
        $this->players = $players;

        return $this;
    }

    public function addPlayer(Player $player) {
        $this->players[] = $player;

        return $this;
    }
}