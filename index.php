<?php

namespace BB;

use BB\Player;
use BB\Race;
use BB\Ability;
use BB\Trait;

$races = array(
    Race::TYPE_HUMAN => new Race(Race::TYPE_HUMAN),
    Race::TYPE_ORC => new Race(Race::TYPE_ORC),
    Race::TYPE_DWARF => new Race(Race::TYPE_DWARF),
    Race::TYPE_SKAVEN => new Race(Race::TYPE_SKAVEN),
    Race::TYPE_ELVE => new Race(Race::TYPE_ELVE),
);

$traits = array(
    Trait::TYPE_AGILITY => new Trait(Trait::TYPE_AGILITY),
    Trait::TYPE_ARMOR => new Trait(Trait::TYPE_ARMOR),
    Trait::TYPE_STRENGTH => new Trait(Trait::TYPE_STRENGTH),
    Trait::TYPE_MOVMENT => new Trait(Trait::TYPE_MOVMENT),
);

$players = array(
    Race::TYPE_HUMAN => array(
        array(
            'name' => 'Luigi di Passela',
            'abilities' => array(
                array(
                    'race' => null, 
                    'isAround' => false,
                    'isTeam' => true,
                    'isRandom' => false, 
                    'when' => Ability::WHEN_KO,
                    'trait' => Trait::TYPE_STRENGTH
                ),
                array(
                    'race' => Race::TYPE_HUMAN, 
                    'isAround' => false,
                    'isTeam' => true,
                    'isRandom' => false, 
                    'when' => Ability::WHEN_START,
                    'trait' => Trait::TYPE_MOVMENT
                ),
                array(
                    'race' => Race::TYPE_ORC, 
                    'isAround' => false,
                    'isTeam' => true,
                    'isRandom' => false, 
                    'when' => Ability::WHEN_START,
                    'trait' => Trait::TYPE_AGILITY
                ),
            )
        ),
    )
);

foreach ($players as $race_type => $player) {
    
    $p = new Player();
    $p->setRace($races[$race_type])
      ->setName($player['name']);

    foreach ($player['abilities'] as $ability) {
        $a = new Ability();
        $a->setRace($races[$ability['race']])
          ->setIsAround($ability['isAround'])
          
        $p->addAbility($a);
    }
}

$p = new Player();
$p->setName('Luigi di Passela')
  ->setRace($humanRace);


?>