<?php

namespace BB;

use BB\Race;
use BB\Trait;

class Ability {

    const WHEN_START = 0;
    const WHEN_HIT = 1;
    const WHEN_KO = 2;
    const WHEN_THROW = 3;
    const WHEN_RECEIVE = 4;

    /**
     * @var integer
     */
    private $race;

    /**
     * @var integer
     */
    private $trait;

    /**
     * @var bool
     */
    private $isAround = false;

    /**
     * @var bool
     */
    private $isTeam = false;

    /**
     * @var bool
     */
    private $isRandom = false;

    /**
     * @var bool
     */
    private $isBonus = false;

    /**
     * @var integer
     */
    private $when;



    /**
     * Gets the value of race.
     *
     * @return Race
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Sets the value of race.
     *
     * @param Race $race the race
     *
     * @return self
     */
    public function setRace(Race $race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Gets the value of isAround.
     *
     * @return bool
     */
    public function getIsAround()
    {
        return $this->isAround;
    }

    /**
     * Sets the value of isAround.
     *
     * @param bool $isAround the is around
     *
     * @return self
     */
    public function setIsAround($isAround)
    {
        $this->isAround = $isAround;

        return $this;
    }

    /**
     * Gets the value of isTeam.
     *
     * @return bool
     */
    public function getIsTeam()
    {
        return $this->isTeam;
    }

    /**
     * Sets the value of isTeam.
     *
     * @param bool $isTeam the is team
     *
     * @return self
     */
    public function setIsTeam($isTeam)
    {
        $this->isTeam = $isTeam;

        return $this;
    }

    /**
     * Gets the value of isRandom.
     *
     * @return bool
     */
    public function getIsRandom()
    {
        return $this->isRandom;
    }

    /**
     * Sets the value of isRandom.
     *
     * @param bool $isRandom the is random
     *
     * @return self
     */
    public function setIsRandom($isRandom)
    {
        $this->isRandom = $isRandom;

        return $this;
    }

    /**
     * Gets the value of isBonus.
     *
     * @return bool
     */
    public function getIsBonus()
    {
        return $this->isBonus;
    }

    /**
     * Sets the value of isBonus.
     *
     * @param bool $isBonus the is bonus
     *
     * @return self
     */
    public function setIsBonus($isBonus)
    {
        $this->isBonus = $isBonus;

        return $this;
    }

    /**
     * Gets the value of when.
     *
     * @return integer
     */
    public function getWhen()
    {
        return $this->when;
    }

    /**
     * Sets the value of when.
     *
     * @param integer $when the when
     *
     * @return self
     */
    public function setWhen($when)
    {
        $this->when = $when;

        return $this;
    }

    /**
     * Gets the value of trait.
     *
     * @return Trait
     */
    public function getTrait()
    {
        return $this->trait;
    }

    /**
     * Sets the value of trait.
     *
     * @param Trait $trait the trait
     *
     * @return self
     */
    public function setTrait(Trait $trait)
    {
        $this->trait = $trait;

        return $this;
    }
}