<?php

namespace BB;

use BB\Race;
use BB\Ability;

class Player 
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var Race
     */
    private $race;

    /**
     * @var array
     */
    private $abilities;

    /**
     * Gets the value of name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the value of name.
     *
     * @param string $name the name
     *
     * @return self
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Gets the value of race.
     *
     * @return Race
     */
    public function getRace()
    {
        return $this->race;
    }

    /**
     * Sets the value of race.
     *
     * @param Race $race the race
     *
     * @return self
     */
    public function setRace(Race $race)
    {
        $this->race = $race;

        return $this;
    }

    /**
     * Gets the value of abilities.
     *
     * @return array
     */
    public function getAbilities()
    {
        return $this->abilities;
    }

    /**
     * Sets the value of abilities.
     *
     * @param array $abilities the abilities
     *
     * @return self
     */
    public function setAbilities(array $abilities)
    {
        $this->abilities = $abilities;

        return $this;
    }

    public function addAbility(Ability $ability) {
        $this->abilities[] = $ability;
        return $this;
    }
}